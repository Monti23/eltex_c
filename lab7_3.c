#include <sys/types.h>
#include <sys/stat.h>
#include <wait.h>
#include <fcntl.h> 
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

int main(int argc, char *argv[]) {
	if (argc < 2) {
			printf("Usage: ./hunger_games int\n");
			exit(-1); 
		}		
	int n = atoi(argv[1]);
	pid_t pid_val;
	pid_t pid_child[n];				// массив доч пидов
	int* pid;
	memset(pid_child, 0, sizeof(int)*n);
	pid_t pid_p = getpid();	
	int fd_child[n][2], fd_parents[n][2];
	int readbuf;
	
	for(int i = 0; i < n; i++){		// создание доч пр и пайпов
		pipe(fd_child[i]);
		pipe(fd_parents[i]);
		pid_val = fork();			// ветвление
		if(pid_val == 0) {	
			close(fd_child[i][0]);
			close(fd_parents[i][1]);
			break;		
			}
			else {	
				close(fd_child[i][1]);
				close(fd_parents[i][0]);
				pid_child[i] = pid_val;									
			}
	}
	if(getpid() == pid_p) {
		for(int k=0; k<n;k++) {			
		write(fd_parents[k][1], pid_child, sizeof(int));
		read(fd_child[k][0],  &readbuf, sizeof(int));
		printf("parents: kill %d", readbuf);
		}
	}
	else {
		
		read(fd_parents[j][0], pid, sizeof(int));
		for(int j=0; j<n; j++) {
		int k = rand()%n;
		if(pid[k] != pid_p && pid[k]!=0 && pid[k]!= getpid()) {
			kill(pid[k], SIGKILL);
			write(fd_child[j][1],  &pid[k], sizeof(int));
			pid[k] =0;
		}
		}
	}
}
