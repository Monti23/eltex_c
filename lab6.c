#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <sys/types.h>
#include <sys/wait.h>

int main(int argc, char *argv[])
{
	
	if(argc < 2)
		{	printf("Error: too few arguments. ./hunger_games <int>\n");
			exit(-1);		}
			
	int n = atoi(argv[1]);

	pid_t pid[n];	
	pid_t pid_val;	
	pid_t pid_p = getpid();	

	for(int i =0; i < n; i++)
	{
	pid_val = fork();	
		if(pid_val == 0)
			{	break;		}
			else
			{	pid[i] = pid_val;
				}
	}
	if(getpid() == pid_p)
	{
	for(int i =0; i < n; i++)
	{printf("пид %d, %d\n", i, pid[i]);
		}} 
	int count = n;	
	for(int i=0;i<n;i++)
	{	
		if( getpid() == pid_p)
			{
//			printf("parent %d\n", getpid());
			pid_val = wait(NULL);
			printf("kill pid %d\n", pid_val);
			count --;
			if(count == 0)
				{ printf("win %d", pid_val);
					}
		}
		else
			{
			srand(time(NULL)); 	
			sleep(rand()%5);
			int k = rand()%4;				// ????
			if(pid[k] != pid_p && pid[k] != 0 && pid[k] != getpid())
				{			
				kill(pid[k], SIGKILL);
				pid[k] = 0;
				}			
			}	
	}		
	return(0);
}
