#include <stdio.h>
#include <string.h>
#include <stdlib.h>

struct book
{
char name[25];
int year;
int page;
float prise;
};

struct book* read_book(struct book *b)
{
	printf("Введите	название книги:");
	scanf(" %s", b->name);

	printf("Введите год издания:");
	scanf(" %d", &b->year);

	printf("Введите количество страниц:");
	scanf(" %d", &b->page);

	printf("Введите стоимость:");
	scanf(" %f", &b->prise);
	return b;
}

void show_book (int count, struct book *b[count])
{
	for(int i=0; i<count; i++)
		{			
		printf("%s, %d, ", b[i]->name, b[i]->year); 
		printf("%d, %.2f \n", b[i]->page, b[i]->prise);
		}		
}

struct book* sort_book (int count, struct book *b[count])
{
	struct book *temp = (char*)malloc(25*sizeof(char));
	int inf;

	for(int i=0; i<count; i++)
		{
		for(int j=0; j<count-i-1; j++)
		{
		inf = strcmp(b[j]->name, b[j+1]->name);
		if( inf > 0)
			{
			temp = b[j];
			b[j] = b[j+1];
			b[j+1] = temp;
			}	
		}}
	free(temp);
	return b;
}

int main (int argc, char* argv[])
{
	int count = 4;
	printf("Введите количество книг: ");
	scanf("%d", &count);		
	
struct book** b = (struct book**)malloc(sizeof(struct book**)*count);
	for(int i=0; i<count; i++) 
		{
		b[i] = (struct book*) malloc (sizeof(struct book));
		printf("%d.\n", i+1);
		b[i] = read_book(b[i]);
		}

	b = sort_book(count, b);
	show_book(count, b);

	for (int i=0; i<count; i++) free(b[i]);
	free(b);

	return 0;
}







